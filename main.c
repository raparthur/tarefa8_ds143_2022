#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

ArvoreRB * rot_esq(ArvoreRB * no){
    ArvoreRB * x = no->dir;
    no->dir = x->esq;
    x->esq = no;
    x->cor = no->cor;
    no->cor = RED;
    return x;
}

ArvoreRB * rot_dir(ArvoreRB * no){
    ArvoreRB * x = no->esq;
    no->esq = x->dir;
    x->dir = no;
    x->cor = no->cor;
    no->cor = RED;
    return(x);
}

ArvoreRB * flip_cor(ArvoreRB * no){
    no->cor = RED;
    no->esq->cor = BLACK;
    no->esq->cor = BLACK;
    return no;
}
//somente conta arestas pretas
int altura_arvore(ArvoreRB *a){
    if (a == NULL)
        return -1;
    else {

    int e = altura_arvore(a->esq);
    int d = altura_arvore(a->dir);
    if(e > d)
        if(a->esq && a->esq->cor == RED){
            return e;
        } else {
            return e + 1;
        }

    else
        if(a->dir && a->dir->cor == RED){
            return d;
        } else {
            return d + 1;
        }
    }
}

int eh_arvore_rb(ArvoreRB * no){
    if(!no){
        return 1;
    }
    if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->esq->esq)){
        return 0;
    }
    if(eh_no_vermelho(no->dir)){
        return 0;
    }
    if(altura_arvore(no->esq) != altura_arvore(no->esq)){
        return 0;
    }
    if(!eh_arvore_rb(no->esq) || !eh_arvore_rb(no->dir)){
        return 0;
    }

    return 1;
}


ArvoreRB * inserir(ArvoreRB * no, int v){
    if(no == NULL){
        no = (ArvoreRB*)malloc(sizeof(ArvoreRB));
        no->info= v;
        no->esq = no->dir = NULL;
        no->cor = RED;
        return no;
    }
    if(v < no->info){
        no->esq = inserir(no->esq, v);
    }
    else{
        no->dir = inserir(no->dir, v);
    }

    int count =0;
    while(!eh_arvore_rb(no)){
        printf("\n>>>comecou loop pela %d vez\n",++count);
        if(eh_no_vermelho(no->dir) && !eh_no_vermelho(no->esq)){
            printf("rot esq , ");
            no = rot_esq(no);
        }
        if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->esq->esq)){
            printf("rot dir , ");
            no = rot_dir(no);
        }
        if(eh_no_vermelho(no->esq) && eh_no_vermelho(no->dir)){
            printf("flip , ");
            no = flip_cor(no);
        }
        printf("\n<<<saiu loop \n");
    }
    return no;
}

ArvoreRB* remover(ArvoreRB *a, int x){
  ArvoreRB * aux, * pai_aux;
  int filhos = 0,tmp;

  if(!a)
    return(NULL);

  if(a->info < x)
    a->dir = remover(a->dir,x);
  else if(a->info > x)
    a->esq = remover(a->esq,x);
  else{
    if(a->esq)
      filhos++;
    if(a->dir)
      filhos++;

    if(filhos == 0){
      free(a);
      return(NULL);
    }
    else if(filhos == 1){
      aux = a->esq ? a->esq : a->dir;
      free(a);
      return(aux);
    }
    else{
      aux = a->esq;
      pai_aux = a;
      while(aux->dir){
            pai_aux = aux;
            aux = aux->dir;
      }
      tmp = a->info;
      a->info = aux->info;
      aux->info = tmp;
      pai_aux->dir = remover(aux,tmp);
      while(!eh_arvore_rb(a)){

        if(eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq)){

            a = rot_esq(a);
        }
        if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->esq->esq)){

            a = rot_dir(a);
        }
        if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->dir)){

            a = flip_cor(a);
        }
      }
      return(a);
    }
  }

  return(a);
}

//FINALMENTE CONSEGUI RESOLVER SEM COLAR ALGO DA INTERNET. DEUS QUEIRA QUE TEJE CERTO :0
int main(){
  //******montar arvore RB manualmente (exemplo da pg 48) *****
  ArvoreRB ta;
  ta.cor = BLACK;
  ta.info = 1;
  ta.dir = NULL;
  ta.esq = NULL;

  ArvoreRB td;
  td.cor = BLACK;
  td.info = 4;
  td.dir = NULL;
  td.esq = NULL;

  ArvoreRB tc;
  tc.cor = RED;
  tc.info = 3;
  tc.dir = &td;
  tc.esq = &ta;

  ArvoreRB tg;
  tg.cor = RED;
  tg.info = 7;
  tg.dir = NULL;
  tg.esq = NULL;

  ArvoreRB tj;
  tj.cor = BLACK;
  tj.info = 10;
  tj.dir = NULL;
  tj.esq = &tg;

  ArvoreRB te;
  te.cor = BLACK;
  te.info = 5;
  te.dir = &tj;
  te.esq = &tc;
  ArvoreRB *a = &te;
  //****** arvore RB incorreta  *****
  ArvoreRB na;
  na.cor = BLACK;
  na.info = 1;
  na.dir = NULL;
  na.esq = NULL;

  ArvoreRB nd;
  nd.cor = RED;
  nd.info = 4;
  nd.dir = NULL;
  nd.esq = NULL;

  ArvoreRB nc;
  nc.cor = RED;
  nc.info = 3;
  nc.dir = &nd;
  nc.esq = &na;

  ArvoreRB nf;
  nf.cor = BLACK;
  nf.info = 6;
  nf.dir = NULL;
  nf.esq = NULL;

  ArvoreRB ng;
  ng.cor = RED;
  ng.info = 7;
  ng.dir = NULL;
  ng.esq = &nf;

  ArvoreRB nj;
  nj.cor = BLACK;
  nj.info = 10;
  nj.dir = NULL;
  nj.esq = &ng;

  ArvoreRB ne;
  ne.cor = BLACK;
  ne.info = 5;
  ne.dir = &nj;
  ne.esq = &nc;

  ArvoreRB *b = &ne;
  //**********************************

  //TESTANDO FUNCOES AUXILIARES
  printf("\n-------- ARVORE RB 'a':---------\n");
  in_order(a);
  printf("\n-------- ARVORE NAO RB 'b':---------\n");
  in_order(b);

  printf("\n-------- ALTURA 'a':%d--------\n", altura_arvore(a));
  printf("\n-------- ALTURA 'b':%d--------\n", altura_arvore(b));

  printf("\n-------- ARVORE 'a' eh RB: %d-------\n",eh_arvore_rb(a));
  printf("\n-------- ARVORE 'b' eh RB: %d-------\n",eh_arvore_rb(b));


  //TESTANDO INSERIR MANTENDO ESTRUTURA RB
  ArvoreRB* c;
  c = inserir(NULL,9);
     c = inserir(c,4);
     c = inserir(c,5);
     c = inserir(c,1);
     c = inserir(c,7);
     c = inserir(c,8);

  printf("\n-------- ARVORE RB construida com a funcao inserir criada:---------\n");
  in_order(c);

  printf("\n-------- ARVORE construida eh RB: %d-------\n",eh_arvore_rb(c));

  //TESTANDO REMOVER MANTENDO ESTRUTURA RB
  printf("\n-------- ARVORE CONTRUIDA REMOVIDO 4:-------\n");
  c = remover(c,4);
  in_order(c);

  printf("\n-------- ARVORE CONTRUIDA CONTINUA SENDO RB: %d-----\n",eh_arvore_rb(c));


    return 0;
}
